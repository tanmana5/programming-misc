#!/bin/python3

import math
import os
import random
import re
import sys


# Complete the digitSum function below.
def digitSum(n, k):
    sum_digs = 0
    for c in str(n):
        sum_digs += (int(c))
    return calc_sum(sum_digs*k)


def calc_sum(p):
    if p < 10:
        return p
    else:
        sum_digs = 0
        for c in str(p):
            sum_digs += (int(c))
        return calc_sum(sum_digs)


if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')
    nk = input().split()
    n = nk[0]
    k = int(nk[1])
    result = digitSum(n, k)
    fptr.write(str(result) + '\n')
    fptr.close()
